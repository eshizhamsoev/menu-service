import {createApp} from 'vue';
import {createPinia} from 'pinia'
import Maska from 'maska';
import VueNextSelect from 'vue-next-select';
import App from './App.vue';
import './assets/scss/app.scss';
import 'vue-next-select/dist/index.css';
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Maska)
app.mount('#app');
app.component('VueSelect', VueNextSelect)
