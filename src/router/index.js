import {createRouter, createWebHistory} from 'vue-router';
import HomeView from '../views/HomeView.vue';

// console.log(import.meta.env.BASE_URL);

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/novelty',
      name: 'novelty',
      component: () => import('../views/NoveltyView.vue'),
    },
    {
      path: '/product/:productId',
      name: 'product',
      props: true,
      component: () => import('../views/ProductView.vue'),
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import('../views/CartView.vue'),
    },
    {
      path: '/order',
      name: 'order',
      component: () => import('../views/OrderView.vue'),
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    return {top: 0}
  }
});

export default router;
