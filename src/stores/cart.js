import { defineStore } from 'pinia';
import { useMenu } from './menu';

export const useCart = defineStore('cart', {
  state: () => ({
    cart: {
      101: {
        quantity: 10
      },
    },
    cartOptions: [
      {
        name: 'Сервисный сбор',
        price: 19
      },
      {
        name: 'Доставка',
        price: 0
      }
    ],
    deliveryMethods: [
      {
        id: 1,
        name: 'В ресторане'
      },
      {
        id: 2,
        name: 'На вынос'
      },
      {
        id: 3,
        name: 'Доставка'
      }
    ],
    paymentMethods: [
      'Картой', 'Наличными'
    ]
  }),
  actions: {
    addProduct(id, quantity) {
      this.cart[id] = { quantity };
      console.log(this.cart);
    },
    increase(id) {
      this.cart[id].quantity += 1;
    },
    decrease(id) {
      this.cart[id].quantity -= 1;
    },
    removeProduct(id) {
      delete this.cart[id];
    }
  },
  getters: {
    quantity: (state) => (productId) => state.cart[productId] ? state.cart[productId].quantity : 0,
    products(state) {
      const menuStore = useMenu();
      const products = [];
      for (const [id, value] of Object.entries(state.cart)) {
        products.push({
          product: menuStore.product(id),
          quantity: value.quantity
        });
      }
      return products;
    },
    cartPrice(state) {
      let result = 0;
      const menuStore = useMenu();
      for (const [id, value] of Object.entries(state.cart)) {
        const product = menuStore.product(id);
        result += product.price.new * value.quantity;
      }
      return result;
    },
    cost() {
      const menuStore = useMenu();
      return `${this.cartPrice} ${menuStore.currency.symbol}`;
    },
    options(state) {
      return state.cartOptions.map(item => {
        const menuStore = useMenu();
        const { name } = item;
        let { price } = item;
        if (item.price === 0) {
          price = 'Бесплатно';
        } else {
          price = `${item.price} ${menuStore.currency.symbol}`;
        }
        return {
          name,
          price
        };
      });
    },
    total(state) {
      const menuStore = useMenu();
      let result = this.cartPrice;
      for (const value of Object.values(state.cartOptions)) {
        result += value.price;
      }
      return `${result} ${menuStore.currency.symbol}`;
    },
    delivery(state) {
      return state.deliveryMethods;
    },
    payments(state) {
      return state.paymentMethods;
    }
  }
});
