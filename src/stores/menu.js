import { defineStore } from 'pinia';
import burgerImgSm from '../assets/images/products/burgers/burger-sm.jpg';
import burgerImgLarge from '../assets/images/products/burgers/burger-large.jpg';
import saladImgSm from '../assets/images/products/salads/salad-sm.jpg';
import saladImgLarge from '../assets/images/products/salads/salad-large.jpg';
import burgerIcon from '../assets/images/burger.png';
import saladIcon from '../assets/images/salad.png';
import noveltyIcon from '../assets/images/new.png';
import drinkIcon from '../assets/images/drink.png';
import soupIcon from '../assets/images/soup.png';

export const useMenu = defineStore('menu', {
  state: () => ({
    products: [
      {
        id: 101,
        name: 'Бургер с говядиной',
        options: {
          weight: '300 гр.',
          calories: '560 Ккал, 2342 кДж',
          calorie: '295 кал.',
          fats: '30 г / 43%',
          squirrels: '29 г / 58%',
          carbohydrates: '42 г / 16%',
          composition: 'Салат Айсберг мелкой нарезки, лук репчатый резаный восстановленный, огурцы маринованные резаные, приправа для гриля',
        },
        price: {
          new: 350,
          old: 470,
        },
        image: {
          small: burgerImgSm,
          large: burgerImgLarge
        },
        belongCategoryIds: [100, 500],
        categories: [
          {
            name: 'Похожие блюда',
            ids: [102]
          },
          {
            name: 'Рекомендуем',
            ids: [301]
          }
        ]
      },
      {
        id: 102,
        name: 'Бургер «Maximum»',
        options: {
          weight: '300 гр.',
          calories: '560 Ккал, 2342 кДж',
          calorie: '295 кал.',
          fats: '30 г / 43%',
          squirrels: '29 г / 58%',
          carbohydrates: '42 г / 16%',
          composition: 'Салат Айсберг мелкой нарезки, лук репчатый резаный восстановленный, огурцы маринованные резаные, приправа для гриля',
        },
        price: {
          new: 350,
          old: 470,
        },
        image: {
          small: burgerImgSm,
          large: burgerImgLarge
        },
        belongCategoryIds: [100],
        categories: [
          {
            name: 'Похожие блюда',
            ids: [102]
          },
          {
            name: 'Рекомендуем',
            ids: [301]
          }
        ]
      },
      {
        id: 301,
        name: 'Салат с авокадо',
        options: {
          weight: '300 гр.',
          calories: '560 Ккал, 2342 кДж',
          calorie: '295 кал.',
          fats: '30 г / 43%',
          squirrels: '29 г / 58%',
          carbohydrates: '42 г / 16%',
          composition: 'Салат Айсберг мелкой нарезки, лук репчатый резаный восстановленный, огурцы маринованные резаные, приправа для гриля',
        },
        price: {
          new: 350,
          old: 470,
        },
        image: {
          small: saladImgSm,
          large: saladImgLarge
        },
        belongCategoryIds: [300],
        categories: [
          {
            name: 'Похожие блюда',
            ids: [102]
          },
          {
            name: 'Рекомендуем',
            ids: [301]
          }
        ]
      },
      {
        id: 302,
        name: 'Салат «Греческий»',
        options: {
          weight: '300 гр.',
          calories: '560 Ккал, 2342 кДж',
          calorie: '295 кал.',
          fats: '30 г / 43%',
          squirrels: '29 г / 58%',
          carbohydrates: '42 г / 16%',
          composition: 'Салат Айсберг мелкой нарезки, лук репчатый резаный восстановленный, огурцы маринованные резаные, приправа для гриля',
        },
        price: {
          new: 350,
          old: 470,
        },
        image: {
          small: saladImgSm,
          large: saladImgLarge
        },
        belongCategoryIds: [300, 500],
        categories: [
          {
            name: 'Похожие блюда',
            ids: [102]
          },
          {
            name: 'Рекомендуем',
            ids: [301]
          }
        ]
      },
    ],
    categories: [
      {
        id: 100,
        name: 'Burgers',
        icon: burgerIcon
      },
      {
        id: 200,
        name: 'Drinks',
        icon: drinkIcon
      },
      {
        id: 300,
        name: 'Salads',
        icon: saladIcon
      },
      {
        id: 400,
        name: 'Soups',
        icon: soupIcon
      },
      {
        id: 500,
        name: 'Novelty',
        icon: noveltyIcon
      },
    ],
    paymentCurrency: {
      code: 'RUB',
      symbol: '₽',
    }
  }),
  getters: {
    menuCategories: (state) => state.categories,
    productCategories: (state) => (productId) => state.products.find(product => product.id === Number(productId)).categories,
    categoryProducts: (state) => (categoryId) => state.products.filter(product => product.belongCategoryIds.includes(categoryId)),
    product: (state) => (productId) => state.products.find(product => product.id === Number(productId)),
    selectedProducts: (state) => (productIds) => productIds.map(id => state.products.find(product => product.id === id)),
    currency: (state) => state.paymentCurrency
  }
});
